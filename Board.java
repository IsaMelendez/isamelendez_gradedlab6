public class Board
{
    private Die die1;
    private Die die2;
    public boolean[] tiles;

    public Board()
    {
        this.die1 = new Die();
        this.die2 = new Die();
        this.tiles = new boolean[12];
    }

    public String toString()
    {
        String boardString = "";

        for(int i=0; i<this.tiles.length; i++)
        {
            if(this.tiles[i])
            {
                boardString+="X ";
            }
            else
            {
                boardString+=(i+1) + " ";
            }
        }

        return boardString;
    }

    public boolean playATurn()
    {
        this.die1.roll();
        this.die2.roll();

        System.out.println("Die number 1 has rolled a " + this.die1.getFaceValue() + "!");
        System.out.println("Die number 2 has rolled a " + this.die2.getFaceValue() + "!");

        int sumOfDice = this.die1.getFaceValue() + this.die2.getFaceValue();
        boolean returnBool;

        if(!tiles[sumOfDice-1])
        {
            System.out.println("Closing tile equal to sum: " + sumOfDice);
            tiles[sumOfDice-1]=true;
            returnBool=false;
        }
        else if(!tiles[this.die1.getFaceValue()-1])
        {
            System.out.println("Closing tile equal to roll of die 1: " + this.die1.getFaceValue());
            tiles[this.die1.getFaceValue()-1]=true;
            returnBool=false;
        }
        else if(!tiles[this.die2.getFaceValue()-1])
        {
            System.out.println("Closing tile equal to roll of die 2: " + this.die2.getFaceValue());
            tiles[this.die2.getFaceValue()-1]=true;
            returnBool=false;
        }
        else
        {
            System.out.println("All of the available tiles for these values have already been shut.");
            returnBool=true;
        }
        
        return returnBool;
    }
}
