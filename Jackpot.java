import java.util.Scanner;

public class Jackpot
{
	public static void main (String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		boolean repeat = true;
		
		int wins = 0;
		
		while(repeat)
		{
			System.out.println("Welcome to Jackpot!");
			
			Board myBoard = new Board();
			
			boolean gameOver = false;
			
			int numOfTilesClosed = 0;
			
			while(!gameOver)
			{
				System.out.println(myBoard);
				
				if(!myBoard.playATurn())
				{
					numOfTilesClosed++;
				}
				else 
				{
					gameOver = true;
				}
				
				System.out.println();
			}
			
			if(numOfTilesClosed>=7)
			{
				System.out.println("You have reached the jackpot and won the game!");
				wins++;
				repeat = false;
			}
			else
			{
				System.out.println("You lost. :( Better luck next time!");
				repeat = false;
			}
			
			System.out.println("Would you like to play again?");
			String answer = sc.nextLine();
			answer = answer.toLowerCase();
			
			while(!answer.equals("yes")&&!answer.equals("no"))
			{
				System.out.println("Input yes or no.");
				answer = sc.nextLine();
				answer = answer.toLowerCase();
			}
			
			if(answer.equals("yes"))
			{
				repeat = true;
			}
			else
			{
				repeat = false;
				System.out.println("Thank you for playing! You have won " + wins + " times.");
			}
		}
		
		
	}
}