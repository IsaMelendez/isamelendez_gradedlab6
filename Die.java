import java.util.Random;

public class Die
{
    private int faceValue;
    private Random rollValue;

    public Die()
    {
        this.faceValue = 1;
        this.rollValue = new Random();
    }

    public int getFaceValue()
    {
        return this.faceValue;
    }

    public int roll()
    {
        this.faceValue = rollValue.nextInt(6)+1;
        return this.faceValue;
    }
}